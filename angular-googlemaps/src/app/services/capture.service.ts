import { Captures } from './../models/capture-model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CaptureService {

  private readonly baseUrl = 'localhost:8080';

  constructor(private http:HttpClient) {
  }


  findBy(areaName:string, areaRound:string) {
    return this.http.get<Captures>(`${this.baseUrl}/api/geopos/area/${areaName}/round/${areaRound}`);
  }

}

import { CaptureService } from './services/capture.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { GoogleMap, MapMarker } from '@angular/google-maps';
import { Captures } from './models/capture-model';
import { catchError, Observable, of } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {

  center: google.maps.LatLngLiteral = { lat: -21.28669206124243, lng: -45.412200815434595 };

  zoom = 17;

  mapOptions: google.maps.MapOptions = {
    center: this.center,
    mapTypeId: 'hybrid',
  }

  markerOptions: google.maps.MarkerOptions = {draggable: false};

  markerPositions: google.maps.LatLngLiteral[] = [];

  addMarker(event: google.maps.MapMouseEvent) {
    let pos = event.latLng;
    alert( pos );

    if (pos) {
      let posJson = pos?.toJSON();
      console.log( posJson );
      this.markerPositions.push( posJson ) ;
    }

  }


  constructor(private captureService: CaptureService){

  }


  captures$ = new Observable<Captures>();

  ngOnInit(): void {
     this.captures$ = this.captureService.findBy('Lavoura Amoreira', '1')
      .pipe(
        catchError(e => {
          this.onError('Erro ao buscar captures: ' + e.message);
          return of([]);
        })
      );
  }

  onError(msgErr: string) {
    alert( msgErr );
  }



}

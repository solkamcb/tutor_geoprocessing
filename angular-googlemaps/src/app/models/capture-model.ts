

export interface Capture {
    id: number;
    lat: string;
    lng: string;
    total: number;
    localDateTimeSource: string;
    localDateTimeTarget: string;
    areaName: string;
    areaRound: string;
}


export type Captures = Array<Capture>;
